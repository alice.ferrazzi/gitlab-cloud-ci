# Copyright (c) Siemens AG, 2020
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

let
  _rev = "c59ea8b8a0e7f927e7291c14ea6cd1bd3a16ff38";
  pkgs = import (builtins.fetchTarball {
    name = "nixpkgs-unstable-2020-08-31";
    url = "https://github.com/nixos/nixpkgs/archive/${_rev}.tar.gz";
    sha256 = "1ak7jqx94fjhc68xh1lh35kh3w3ndbadprrb762qgvcfb8351x8v";
  }) { };

in with pkgs;

mkShell {

  buildInputs = [ python3 poetry ];

}
