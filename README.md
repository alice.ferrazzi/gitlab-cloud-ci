# Gitlab Custom Cloud CI

This project creates a Kubernetes-based CI-infrastructure for Gitlab from scratch.
Clusters can run on either AWS or on a Linux host (ssh root access required).

Objectives:

* Allow running **privileged containers** for CI pipelines (unlike the default Gitlab runners). This is the main reason this project exists.
* Scale horizontally: run an (almost) arbitrary number of CI jobs in parallel
* Scale dynamically: scale up and down based on the current workload situation

## Getting Started (AWS)

1. `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` are needed to create the necessary AWS resources.
  The following permissions must be associated with the API keys:
    * `AmazonEC2FullAccess`
    * `AmazonRoute53FullAccess`
    * `AmazonS3FullAccess`
    * `IAMFullAccess`
    * `AmazonVPCFullAccess`

See [github.com/kubernetes/kops/blob/master/docs/getting_started/aws.md](https://github.com/kubernetes/kops/blob/master/docs/getting_started/aws.md)  for detailed instructions.

```bash
$ export AWS_ACCESS_KEY_ID=...
$ export AWS_SECRET_ACCESS_KEY=...
```

2. Create `config.json`:

```bash
# optional step, but recommended for security reasons:
$ mkdir -p secrets/ssh
# do not set a passphrase
$ ssh-keygen -f secrets/ssh/aws_ssh_key

$ cp config.json.sample config.json
# edit config.json and adjust it to your needs
```

3. Launch Docker container:

```bash
$ make shell
```

4. Create cluster on AWS:

```bash
$ gitlabci create aws

# optionally, deploy AWS autoscaler add-on:
$ gitlabci autoscaler deploy --min-size 0 --max-size 3
```

5. Deploy Gitlab runner:

Go to your projects CI setup page (Settings -> CI/CD -> Runners) and grab the token for Runner registration.

Then, for each runner which you want to deploy, create a local copy of `share/k8s/gitlab-runner/values.yaml`,
adjust the configuration and deploy the runner, e.g.

```bash
$ cp share/k8s/gitlab-runner/values.yaml share/k8s/gitlab-runner/example-runner.yaml
$ gitlabci runner deploy \
    --config=share/k8s/gitlab-runner/example-runner.yaml \
    example-runner
```

Hint: You can "Disable Shared Runners" in the Gitlab UI to test your custom runner.

## Cleanup

* To purge a Gitlab runner from your cluster, run

```bash
$ gitlabci runner purge example-runner
```

Hint: Use `gitlabci runner list` to get an overview of all deployed runners.

* To tear down the cluster and all resources associated with it:

```bash
$ gitlabci destroy aws
```

## On-Premise Setup (SSH)

Prerequisites:

- The SSH keys used by `gitlabci` need to be in PEM format.
  You can convert keys with: `ssh-keygen -p -m PEM -f <private_key>` or generate a new one with `ssh-keygen -t rsa -m PEM`.
  The file `~/.ssh/config` is partially supported to determine SSH connection settings.
- Ensure iptables tooling does not use the nftables backend: See [legacy iptables](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#ensure-iptables-tooling-does-not-use-the-nftables-backend).
- [docker-ce](https://kubernetes.io/docs/setup/production-environment/container-runtimes/#docker)
- [kubeadm, kubectl, kubelet](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#installing-runtime)
- Swap must be deactivated (`swapoff -a`)

## Add-Ons

### Dashboard

A Kubernetes dashboard can be deployed as well and accessed as follows:

```bash
$ gitlabci dashboard deploy
$ kubectl proxy --address=0.0.0.0 &
$ gitlabci dashboard get-token
```

Now open
http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:https/proxy/
in your browser and use the above token to login.

### Monitoring

Grafana can be deployed for in-depth monitoring as follows:
```bash
$ gitlabci monitoring deploy
$ export POD_NAME=$(kubectl get pods --namespace monitoring -l "app.kubernetes.io/name=grafana,app.kubernetes.io/instance=grafana" -o jsonpath="{.items[0].metadata.name}")
$ kubectl port-forward --address 0.0.0.0 -n monitoring ${POD_NAME} 3030:3000 &
$ gitlabci monitoring get-admin-password
```
Now open http://localhost:3030 in your browser and use the password from `gitlabci monitoring get-admin-password`
together with the username 'admin'.

This will increase the Memory requirements of the master by around 500MB.

### Dashboard

The [default dashboard](https://grafana.com/grafana/dashboards/6417) can be found at http://localhost:3030/dashboards 

## Testing

To test the on-premise setup you can use the provided Vagrantfile in `./test/siemens/onpremise/`.
See [README.vagrant.md](./tests/siemens/onpremise-vm/README.vagrant.md) for further instructions.

## Advanced

See [ADVANCED.md](./ADVANCED.md).

## References

* https://docs.gitlab.com/runner/executors/kubernetes.html
* https://docs.gitlab.com/runner/install/kubernetes.html
* https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container/
* https://github.com/kubernetes/autoscaler
