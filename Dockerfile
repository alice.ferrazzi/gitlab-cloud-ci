# Copyright (c) Siemens AG, 2020
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

FROM python:3.8-alpine

COPY . /src

WORKDIR /src

ENV K8S_VERSION 1.18.14
ENV KOPS_VERSION v1.18.2

RUN apk add --no-cache zsh openssl openssh libffi ca-certificates make ansible sed \
  && apk add --no-cache --virtual build-dependencies gcc musl-dev openssl-dev libffi-dev curl bash \
  && sed -i -e 's#/root:/bin/ash#/root:/bin/zsh#' /etc/passwd \
  && curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python \
  && /root/.poetry/bin/poetry install --no-dev --no-root \
  && curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash \
  && curl -sL https://dl.k8s.io/v${K8S_VERSION}/kubernetes-client-linux-amd64.tar.gz | tar x -z -C /tmp && mv /tmp/kubernetes/client/bin/kubectl /usr/local/bin/kubectl \
  && curl -o /usr/local/bin/kops -sL https://github.com/kubernetes/kops/releases/download/${KOPS_VERSION}/kops-linux-amd64 \
  && chmod 755 /usr/local/bin/kops \
  && kubectl completion zsh > /usr/share/zsh/site-functions/_kubectl  \
  && helm completion zsh > /usr/share/zsh/site-functions/_helm \
  && kops completion zsh > /etc/zsh/kops \
  && /root/.poetry/bin/poetry completions zsh > /usr/share/zsh/site-functions/_poetry \
  && cp share/completion/zsh/_gitlabci /usr/share/zsh/site-functions/_gitlabci \
  && cp share/zshrc /root/.zshrc \
  && apk del build-dependencies

EXPOSE 8001/tcp

ENV PATH /src:/root/.poetry/bin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
ENV SHELL /bin/zsh

ENTRYPOINT ["/bin/zsh", "-c"]
