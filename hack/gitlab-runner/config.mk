# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0

GITLAB_RUNNER_VERSION ?= v12.5.0
DOCKER_IMAGE = registry.gitlab.com/cip-project/cip-testing/gitlab-cloud-ci/gitlab-runner

