#!/usr/bin/env bash
set -euo pipefail

WHITELIST=$(cat <<EOF
ADVANCED.md
COPYING
LICENSE
README.md
config.json.sample
hack/gitlab-runner/README.md
hack/gitlab-runner/files/0001-executors-kubernetes-support-custom-entrypoints.patch
poetry.lock
requirements-frozen.txt
share/k8s/autoscaler/asg-policy.json
tests/siemens/onpremise-vm/README.vagrant.md
EOF
)

FILES_WITHOUT_SPDX=$(git grep --files-without-match SPDX)

BAD_FILES=$(comm -23 \
  <(echo "$FILES_WITHOUT_SPDX" | sort | uniq) \
  <(echo "$WHITELIST" | sort | uniq))

[[ -z "$BAD_FILES" ]] || {
  echo "$BAD_FILES"
  exit 1
}
