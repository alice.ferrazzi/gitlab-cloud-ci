# Advanced Topics

## Cluster Management & Fine-Tuning

Typically you will want to fine-tune certain cluster settings (e.g. node instance type) because the current settings do not properly fit your workload.

`gitlabci` only exposes a (currently small) subset of the available cluster management commands.

However, `make shell` will start a Docker container in which many tools such as `helm` are available (and functional) without any extra configuration,
e.g. commands like `helm list` just work out of the box.

### kubectl

**AWS**: In case you have lost your local kube config file, you can simply re-create it using  `kops export kubecfg "${CLUSTER_NAME}.k8s.local"`.
(This file is needed to invoke `kubectl`.)

### Modify Gitlab Runners

For instance, to upgrade a Gitlab Runner to the latest version or modify some configuration settings:

```bash
$ helm repo update
# remove '--dry-run' if output looks sane
$ helm upgrade isar gitlab/gitlab-runner --dry-run --reuse-values -f ./share/k8s/gitlab-runner/values.yaml
```

### Modify Auto-Scaling Group Configuration

You can adjust the configuration of the auto-scaling group either in the AWS UI or using your favorite command-line tool, e.g.

```bash
$ awless list scalinggroups --format json
$ awless update scalinggroup name=... max-size=10
```

### Modify EC2 instance type for nodes

```bash
kops edit ig --name ${CLUSTER_NAME}.k8s.local nodes
kops update cluster ${CLUSTER_NAME}.k8s.local --yes
```

**Notice:** You have to re-deploy the `cluster-autoscaler` plugin afterwards because kops overrides certain tags in the ASG configuration.

## Custom Docker Registry

In order to use a Docker registry requiring authentication, create the file `./secrets/docker.json` with the following contents:

```json
{
  "auths": {
    "registry.gitlab.com": {
      "auth": "<base64 encoded value of user:token>"
    }
  }
}
```

Then run

```bash
kubectl create secret generic regcred \
    --from-file=.dockerconfigjson=./secrets/docker.json \
    --type=kubernetes.io/dockerconfigjson \
    --namespace gitlab
```

See https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/ for further details.

