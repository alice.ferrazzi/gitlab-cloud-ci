# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#
---
- hosts: all
  remote_user: vagrant
  become_method: sudo
  become: true
  tasks:
  - name: Install prerequisites
    apt:
      name: ['apt-transport-https', 'ca-certificates', 'curl', 'gnupg2' ,'software-properties-common', 'iptables', 'arptables', 'ebtables']
      update_cache: true
  - name: Use legacy iptables for k8s
    alternatives:
      name: iptables
      path: /usr/sbin/iptables-legacy
  - name: Use legacy ip6tables for k8s
    alternatives:
      name: ip6tables
      path: /usr/sbin/ip6tables-legacy
  - name: use legacy arptabels for k8s
    alternatives:
      name: arptables
      path: /usr/sbin/arptables-legacy
  - name: Use legacy ebtables for k8s
    alternatives:
      name: ebtables
      path: /usr/sbin/ebtables-legacy
  - name: Add Docker GPG key
    apt_key: url=https://download.docker.com/linux/debian/gpg
  - name: Add Docker APT repository
    apt_repository:
      repo: deb [arch=amd64] https://download.docker.com/linux/{{ansible_distribution|lower}} {{ansible_distribution_release}} stable
  - name: Install Docker
    apt:
      name: ['docker-ce=5:19.03.12~3-0~debian-buster']
      update_cache: true

  - name: Add K8s GPG key
    apt_key: url=https://packages.cloud.google.com/apt/doc/apt-key.gpg

  - name: Add K8s APT repository
    apt_repository:
      repo: deb [arch=amd64] https://apt.kubernetes.io/ kubernetes-xenial main

  - name: Install K8s
    apt:
      name: ['kubeadm=1.18.14-00', 'kubectl=1.18.14-00', 'kubelet=1.18.14-00', 'kubernetes-cni=0.8.7-00']
      update_cache: true

  - name: Add vagrant user to docker group
    user:
      name: vagrant
      group: docker

  - name: Remove swapfile from /etc/fstab
    mount:
      name: "{{ item }}"
      fstype: swap
      state: absent
    with_items:
      - swap
      - none

  - name: deactivate swap
    command: swapoff -a
    when: ansible_swaptotal_mb > 0

  - name: Configure node ip
    lineinfile:
      path: /etc/default/kubelet
      create: true
      line: KUBELET_EXTRA_ARGS=--node-ip={{ node_ip }}
  - name: Restart kubelet
    service:
      name: kubelet
      daemon_reload: true
      state: restarted
