# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

VERSION = $(shell grep "^version" pyproject.toml | cut -d= -f2 | tr -d '"[:space:]')
DOCKER_IMAGE = registry.gitlab.com/cip-project/cip-testing/gitlab-cloud-ci
DOCKER ?= docker
PWD = $(shell pwd)
CLUSTER_NAME=$(shell grep '"cluster_name"' config.json | tr -d '",[:space:]' | cut -d: -f2)

default: format lint test

format:
	./hack/format.sh

lint:
	./hack/lint.sh
	./hack/license_check.sh

test:
	python3 -m pytest tests

docker: clean
	$(DOCKER) build --pull --no-cache -t "$(DOCKER_IMAGE):$(VERSION)" .

gitlab-runner:
	make -C ./hack/gitlab-runner build

shell:
	mkdir -p secrets/kube secrets/helm/config secrets/helm/share secrets/ssh
	[ -f secrets/bash_history ] || touch secrets/bash_history
	$(DOCKER) run --rm \
		--volume $(PWD):/src \
		--volume $(PWD)/secrets/ssh:/root/.ssh \
		--volume $(PWD)/secrets/kube:/root/.kube \
		--volume $(PWD)/secrets/helm/config:/root/.config/helm \
		--volume $(PWD)/secrets/helm/share:/root/.local/share/helm \
		--volume $(PWD)/secrets/bash_history:/root/.bash_history \
		--workdir=/src \
		--env AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID) \
		--env AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY) \
		--env HELM_TLS_ENABLE=true \
		--env https_proxy=$(https_proxy) \
		--env http_proxy=$(http_proxy) \
		--env no_proxy=$(no_proxy) \
		--env CLUSTER_NAME=$(CLUSTER_NAME) \
		--env "KOPS_STATE_STORE=s3://$(CLUSTER_NAME)-kops-state-store" \
		--publish 3030:3030 \
		--publish 8001:8001 \
		-it $(DOCKER_IMAGE):$(VERSION) "poetry shell"

clean:
	rm -rf .pytest_cache
	find . -type f -name '*.py[co]' -delete -o -type d -name __pycache__ -delete

.PHONY: format lint test docker shell clean gitlab-runner
