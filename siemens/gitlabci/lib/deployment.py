# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

from sh import helm


def bootstrap_helm():
    helm("repo", "add", "stable", "https://charts.helm.sh/stable")
    helm("repo", "add", "dashboard", "https://kubernetes.github.io/dashboard/")
    helm("repo", "add", "grafana", "https://grafana.github.io/helm-charts")
    helm(
        "repo",
        "add",
        "prometheus",
        "https://prometheus-community.github.io/helm-charts",
    )
    helm("repo", "update")
