# Copyright (c) Siemens AG, 2020
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import os
import logging

if not os.getenv("_GITLABCI_COMPLETE", None):
    from sh import ansible_playbook, ansible_galaxy

log = logging.getLogger(__name__)


def install_role(role):
    """Install the given role with ansible-galaxy"""
    ansible_galaxy("install", role)


def run_playbook(playbook_file, inventory_file):
    """run the given ansible playbook with the given file"""
    log.info(
        "run ansible playbook.\
    If a passphrase protects the ssh key enter the passphrase if requested."
    )
    # put ansible into the foreground for passphrase protected ssh keys
    ansible_playbook("--inventory", inventory_file, playbook_file, "-b", "-k", _fg=True)
