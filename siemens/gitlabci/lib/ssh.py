# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import logging

import os
from io import StringIO
from paramiko import (
    SSHClient,
    SSHConfig,
    ProxyCommand,
    AutoAddPolicy,
    RSAKey,
    DSSKey,
    SSHException,
)

log = logging.getLogger(__name__)


class SSHSession:
    def __init__(self, destination, config_file, ssh_password=None):
        """create a ssh session to host name read the configuration from
           the given configuration file. The options parsed from the file are:
        - ProxyCommand
        - IdentityFile RSA or DES key
        - HostName
        - User
        - Port
        """
        config = SSHConfig()
        ssh_config_file = os.path.expanduser(config_file)
        if os.path.exists(ssh_config_file):
            with open(ssh_config_file) as f:
                config.parse(f)

        ssh_config = config.lookup(destination)
        proxy = ""

        if "proxycommand" in ssh_config:
            proxy = ProxyCommand(ssh_config["proxycommand"])

        key_filename = ssh_config["identityfile"][0]
        ssh_client = SSHClient()
        ssh_client.load_system_host_keys()
        ssh_client.set_missing_host_key_policy(AutoAddPolicy())
        ssh_client.connect(
            hostname=ssh_config["hostname"],
            username=ssh_config["user"],
            port=ssh_config["port"],
            pkey=read_sshkey(key_filename),
            passphrase=ssh_password,
            sock=proxy,
        )
        self.client = ssh_client

    def execute_command(self, command, input_str=None):
        """execute the given command and print output/errors"""
        transport = self.client.get_transport()
        channel = transport.open_session()
        try:
            channel.get_pty()
            file_desc = channel.makefile()
            channel.exec_command(command)
            if input_str:
                stdin = channel.makefile("wb")
                stdin.write(input_str)
                stdin.flush()

            for line in iter(file_desc.readline, ""):
                if "error" in line.casefold():
                    log.error(line)
                elif "warning" in line.casefold():
                    log.warning(line)
                else:
                    log.info(line)
        finally:
            channel.close()

    def close(self):
        """close the ssh session"""
        self.client.close()


def read_sshkey(filename):
    """read the ssh key file and generate the key for the ssh session"""
    keystring = open(filename, "r").read()
    try:
        pkey = RSAKey.from_private_key(file_obj=StringIO(keystring))
        return pkey
    except SSHException:
        try:
            pkey = DSSKey.from_private_key(file_obj=StringIO(keystring))
            return pkey
        except SSHException:
            raise
