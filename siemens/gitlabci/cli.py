# Copyright (c) Siemens AG, 2019 - 2020
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#
import click


@click.group(context_settings=dict(max_content_width=120))
@click.pass_context
def cli(ctx):
    ctx.ensure_object(dict)


@cli.group(name="autoscaler")
@click.pass_context
def autoscaler(ctx):
    """ Manage autoscaler plugin """
    pass


@cli.group(name="dashboard")
@click.pass_context
def dashboard(ctx):
    """ Manage dashboard plugin """
    pass


@cli.group(name="runner")
@click.pass_context
def runner(ctx):
    """ Manage Gitlab runners """
    pass


@cli.group(name="monitoring")
@click.pass_context
def monitoring(ctx):
    """ Manage Monitoring plugin """
    pass


@cli.group(name="hardening")
@click.pass_context
def hardening(ctx):
    """ Security Hardening for a ISAR CI setup """
    pass


@cli.group(name="create")
@click.pass_context
def create(ctx):
    """create a ISAR CI setup"""
    pass


@cli.group("destroy")
@click.pass_context
def destroy(ctx):
    """destroy a ISAR CI setup"""
    pass


@cli.group(name="jobs")
@click.pass_context
def jobs(ctx):
    """ Manage Gitlab jobs """
    pass
