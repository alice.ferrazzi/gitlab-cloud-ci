# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import os
import sys
import click

import coloredlogs

from siemens.gitlabci.cli import cli

if not os.getenv("_GITLABCI_COMPLETE", None):
    from sh import kubectl
    from siemens.gitlabci.cmd.runner.list import _list as runner_list
    from siemens.gitlabci.cmd.jobs.list import _list as jobs_list


@cli.command()
@click.option(
    "--namespace", default="gitlab", help="Use the given namespace", show_default=True
)
def status(namespace):
    """ Show cluster status """

    coloredlogs.set_level("ERROR")

    procs = []
    master_p = kubectl("get", "nodes", "--selector=kubernetes.io/role=master", _bg=True)
    slaves_p = kubectl(
        "get", "nodes", "--selector=kubernetes.io/role!=master", _bg=True
    )
    procs.append(master_p)

    for p in procs:
        p.wait()

    print(79 * "#")
    print("## Masters")
    print(79 * "#")
    print("")
    sys.stdout.write(master_p.stdout.decode("utf-8"))

    print("\n")

    print(79 * "#")
    print("## Slaves")
    print(79 * "#")
    print("")
    sys.stdout.write(slaves_p.stdout.decode("utf-8"))

    print("\n")

    print(79 * "#")
    print("## Gitlab Runners")
    print(79 * "#")
    print("")
    print(runner_list(namespace))

    print("\n")

    print(79 * "#")
    print("## CI Jobs")
    print(79 * "#")
    print("")
    print(jobs_list(namespace))
