# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import os
import tempfile
import logging

import click
import deep_merge
import yaml

from siemens.gitlabci.cli import runner as cli

if not os.getenv("_GITLABCI_COMPLETE", None):
    import sh
    from sh import kubectl, helm

log = logging.getLogger(__name__)


@cli.command()
@click.argument("name")
@click.option("--token", help="Gitlab runner token", required=False)
@click.option(
    "--namespace",
    default="gitlab",
    help="Deploy in the given namespace",
    show_default=True,
)
@click.option(
    "--config",
    default="./share/k8s/gitlab-runner/values.yaml",
    type=click.Path(exists=True, file_okay=True, dir_okay=False),
    help="Path to the configuration file for the gitlab runner",
    show_default=True,
)
@click.option(
    "--chart-version",
    default="0.23.0",
    help="Use the provided Helm chart version",
    show_default=True,
)
def deploy(name, token, namespace, config, chart_version):
    """Deploy a Gitlab runner instance into the cluster"""

    overrides = {}
    if token:
        overrides["runnerRegistrationToken"] = token

    log.info("Reading default values from file %s", config)
    with open(config, mode="r") as cfg_file:
        default_cfg = yaml.safe_load(cfg_file)

    merged_config = deep_merge.merge(
        default_cfg, overrides, merge_lists=deep_merge.append_lists
    )

    with tempfile.NamedTemporaryFile(mode="w") as temp:
        yaml.dump(merged_config, temp)
        temp.flush()

        helm("repo", "add", "gitlab", "https://charts.gitlab.io")
        helm("repo", "update")
        try:
            kubectl("get", "namespaces", namespace)
        except sh.ErrorReturnCode:
            kubectl("create", "namespace", namespace)
        helm(
            "install",
            "gitlab/gitlab-runner",
            "--version={}".format(chart_version),
            "--namespace",
            namespace,
            "--name-template",
            name,
            "--values",
            temp.name,
        )
