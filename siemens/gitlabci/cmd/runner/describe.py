# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import os

import click

from siemens.gitlabci.cli import runner as cli


@cli.command()
@click.argument("runner")
@click.option(
    "--namespace", default="gitlab", help="Use the given namespace", show_default=True
)
def describe(runner, namespace):
    """ Describe a specific Gitlab runner instance """
    os.execlp(
        "kubectl",
        "kubectl",
        "describe",
        "pods",
        "-l",
        "app={}-gitlab-runner".format(runner),
        "--namespace={}".format(namespace),
    )
