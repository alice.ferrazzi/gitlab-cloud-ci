# Copyright (c) Siemens AG, 2020
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#
import os
import click
from siemens.gitlabci.cli import monitoring as cli

if not os.getenv("_GITLABCI_COMPLETE", None):
    from sh import kubectl
    import base64
    import json


@cli.command()
@click.option(
    "--namespace",
    default="monitoring",
    help="Get the password from the given namespace",
    show_default=True,
)
def get_admin_password(namespace):
    """get the generated admin password for grafana"""
    secret_json = kubectl(
        "get", "secret", "--namespace", namespace, "grafana", "-o", "json"
    )
    data = json.loads(str(secret_json))
    print(base64.b64decode(data.get("data").get("admin-password")).decode())
