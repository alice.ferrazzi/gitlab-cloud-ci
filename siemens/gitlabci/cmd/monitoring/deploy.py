# Copyright (c) Siemens AG, 2020
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#
import tempfile
import logging

from string import Template

import os
import click
from siemens.gitlabci.cli import monitoring as cli

if not os.getenv("_GITLABCI_COMPLETE", None):
    import sh
    from sh import kubectl, helm
    import kubernetes
    from siemens.gitlabci.lib.aws import on_aws
    from siemens.gitlabci.lib.kubernetes import configure_kubernetes_client

log = logging.getLogger(__name__)


@cli.command()
@click.pass_context
@click.option(
    "--namespace",
    default="monitoring",
    help="Deploy in the given namespace",
    show_default=True,
)
def deploy(ctx, namespace):
    """deploy monitoring pods"""
    helm("repo", "add", "grafana", "https://grafana.github.io/helm-charts")
    helm(
        "repo",
        "add",
        "prometheus",
        "https://prometheus-community.github.io/helm-charts",
    )
    helm("repo", "update")

    try:
        kubectl("get", "namespaces", namespace)
    except sh.ErrorReturnCode:
        kubectl("create", "namespace", namespace)

    configure_kubernetes_client()

    k8s_client_core = kubernetes.client.CoreV1Api()

    if not on_aws(k8s_client_core):
        kubectl("apply", "-f", "./share/k8s/prometheus/pv-volume.yaml")

    helm(
        "install",
        "--name-template",
        "prometheus",
        "prometheus/prometheus",
        "--namespace",
        namespace,
        "-f",
        "./share/k8s/monitoring/prometheus.yaml",
    )
    with open("./share/k8s/monitoring/grafana.yaml", mode="r") as grafana_file:
        grafana_config = Template(grafana_file.read())

    overwrites = {"namespace": namespace}

    with tempfile.NamedTemporaryFile(mode="w") as temp_grafana_config:
        merged_grafana_config = grafana_config.substitute(overwrites)
        temp_grafana_config.write(merged_grafana_config)
        temp_grafana_config.flush()
        helm(
            "install",
            "--name-template",
            "grafana",
            "grafana/grafana",
            "--namespace",
            namespace,
            "-f",
            temp_grafana_config.name,
        )
