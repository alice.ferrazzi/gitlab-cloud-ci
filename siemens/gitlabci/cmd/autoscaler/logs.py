# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import logging
import os

import click

from siemens.gitlabci.cli import autoscaler as cli

log = logging.getLogger(__name__)


@cli.command()
@click.option("--follow", help="Specify if the logs should be streamed", is_flag=True)
@click.pass_context
def logs(ctx, follow):
    """Purge AWS autoscaler pod"""
    log.info("Getting logs for cluster-autoscaler")
    os.execlp(
        "kubectl",
        "kubectl",
        "logs",
        "deployment/aws-auto-scaler-aws-cluster-autoscaler",
        "--namespace=kube-system",
        "--follow={}".format(str(follow).lower()),
    )
