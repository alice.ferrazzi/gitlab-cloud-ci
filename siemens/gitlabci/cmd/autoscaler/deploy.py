# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import logging
import os

import click

from siemens.gitlabci.cli import autoscaler as cli

if not os.getenv("_GITLABCI_COMPLETE", None):
    import pathlib
    import boto3
    from sh import helm
    from siemens.gitlabci.lib.aws import aws_iam_lookup_policy_arn, check_aws_response

log = logging.getLogger(__name__)


@cli.command()
@click.pass_context
@click.option(
    "--min-size",
    default=0,
    type=int,
    help="Minimum number of available nodes",
    show_default=True,
)
@click.option(
    "--max-size",
    default=2,
    type=int,
    help="Maximum number of available nodes",
    show_default=True,
)
@click.option(
    "--scale-down-unneeded-time",
    default="1m",
    help="How long a node should be unneeded before it is eligible for scale down",
    show_default=True,
)
def deploy(ctx, min_size, max_size, scale_down_unneeded_time):
    """ Deploy AWS autoscaler to dynamically adjust cluster size"""

    # needs to be changed when Kubernetes is upgraded
    chart_version = "8.0.0"

    cfg = ctx.obj["cfg"]
    region = cfg.aws["region"]
    cluster_name = cfg.aws["cluster_name"]
    asg_policy_name = cfg.aws["asg_policy_name"]

    full_cluster_name = "{}.k8s.local".format(cluster_name)

    # kops creates two roles, nodes.$CLUSTER_NAME and masters.$CLUSTER_NAME
    # since the auto-scaler process runs on the master, we choose the latter one
    iam_role = "masters.{}".format(full_cluster_name)
    iam = boto3.client("iam", region_name=region)

    policy_arn = aws_iam_lookup_policy_arn(iam, asg_policy_name)
    if not policy_arn:
        log.info("Policy %s does not yet exist, creating now", asg_policy_name)
        response = iam.create_policy(
            PolicyName=asg_policy_name,
            PolicyDocument=pathlib.Path(
                "./share/k8s/autoscaler/asg-policy.json"
            ).read_text(),
        )
        check_aws_response(response)
        policy_arn = response["Policy"]["Arn"]

    log.info("Attaching IAM policy %s to role %s", policy_arn, iam_role)
    check_aws_response(iam.attach_role_policy(RoleName=iam_role, PolicyArn=policy_arn))

    log.info("Updating configuration of autoscaling group")
    client = boto3.client("autoscaling", region_name=region)

    asg_name = "nodes.{}".format(full_cluster_name)
    check_aws_response(
        client.update_auto_scaling_group(
            AutoScalingGroupName=asg_name,
            MinSize=min_size,
            MaxSize=max_size,
            DesiredCapacity=max(0, min_size),
            DefaultCooldown=10,
        )
    )
    check_aws_response(
        client.create_or_update_tags(
            Tags=[
                {
                    "PropagateAtLaunch": True,
                    "ResourceId": asg_name,
                    "ResourceType": "auto-scaling-group",
                    "Key": "Role",
                    "Value": "gitlab-cloud-ci",
                }
            ]
        )
    )

    log.info("Deploying cluster-autoscaler")
    helm(
        "install",
        "stable/cluster-autoscaler",
        "--version",
        chart_version,
        "--name-template",
        "aws-auto-scaler",
        "--namespace",
        "kube-system",
        "--set",
        (
            "autoscalingGroups[0].name=nodes.{full_cluster_name},"
            "autoscalingGroups[0].maxSize={asg_max_size},"
            "autoscalingGroups[0].minSize={asg_min_size}"
        ).format(
            full_cluster_name=full_cluster_name,
            asg_min_size=min_size,
            asg_max_size=max_size,
        ),
        "--set",
        "awsRegion={}".format(region),
        "--set",
        "extraArgs.scale-down-unneeded-time={}".format(scale_down_unneeded_time),
        "-f",
        "./share/k8s/autoscaler/values.yaml",
    )
