# Copyright (c) Siemens AG, 2019 - 2020
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import logging
import os

from string import Template

import tempfile
import click
from siemens.gitlabci.cli import create as cli

if not os.getenv("_GITLABCI_COMPLETE", None):
    import kubernetes
    from paramiko import SFTPClient
    from siemens.gitlabci.lib.ssh import SSHSession
    from siemens.gitlabci.lib.kubernetes import (
        configure_kubernetes_client,
        add_dedicated_role_to_master,
    )
    from siemens.gitlabci.lib.deployment import bootstrap_helm
    from sh import kubectl

log = logging.getLogger(__name__)


@cli.command()
@click.option(
    "--ssh-config-file",
    default="~/.ssh/config",
    help="location of the ssh configuration file",
    show_default=True,
)
@click.option(
    "--api-server-addr",
    default="192.168.50.10",
    help="outbound address of the api server on the host ",
    show_default=True,
)
@click.option(
    "--calico-config-file",
    default="./share/k8s/calico/calico.yaml",
    help="configuration of the calico network ",
    show_default=True,
)
@click.option(
    "--sudo",
    is_flag=True,
    help="use sudo to create the Kubernetes cluster",
    show_default=False,
)
@click.option(
    "--ask-password", is_flag=True, help="ask for ssh passphrase", show_default=False
)
@click.option(
    "--pod-network-cidr",
    default="10.244.0.0/16",
    help="Classless Inter-Domain Routing for the internal network ",
    show_default=True,
)
@click.argument("destination")
def ssh(
    destination,
    ssh_config_file,
    api_server_addr,
    sudo,
    ask_password,
    pod_network_cidr,
    calico_config_file,
):
    """Creates a ISAR CI setup running on the given host. The destination
    configuration extracted from the given ssh configuration.
    The destination argument must be equal to a 'Host' entry in the ssh
    configuration."""
    create_k8s_cluster(
        destination,
        ssh_config_file,
        api_server_addr,
        sudo,
        ask_password,
        pod_network_cidr,
    )
    # install network
    with open(calico_config_file, mode="r") as cfg_file:
        calico_cfg_file = Template(cfg_file.read())

    overwrites = {"network_ipv4_cidr": pod_network_cidr}

    with tempfile.NamedTemporaryFile(mode="w") as temp:
        merged_cfg = calico_cfg_file.substitute(overwrites)
        temp.write(merged_cfg)
        kubectl("apply", "-f", temp.name)

    # untainted master
    kubectl("taint", "nodes", "--all", "node-role.kubernetes.io/master-")

    configure_kubernetes_client()

    k8s_client_core = kubernetes.client.CoreV1Api()

    for row in k8s_client_core.list_node().items:
        labels = row.metadata.labels
        if "kubernetes.io/role=master" not in labels:
            master_name = row.metadata.name
            log.info(
                "Adding label kubernetes.io/role=master to master node %s", master_name
            )
            k8s_client_core.patch_node(
                master_name, {"metadata": {"labels": {"kubernetes.io/role": "master"}}}
            )

    add_dedicated_role_to_master()
    bootstrap_helm()

    log.info("Success! Your K8s cluster is up and running.")


def create_k8s_cluster(
    destination, config_file, api_server_addr, sudo, ask_password, cidr
):
    """create the k8s cluster with kubeadm and ssh"""
    ssh_password = None
    if ask_password:
        ssh_password = click.prompt(
            "enter ssh passphrase:", hide_input=True, confirmation_prompt=True
        )
    ssh_session = SSHSession(destination, config_file, ssh_password)
    try:
        api_server_settings = (
            ' \
        --apiserver-advertise-address="%(addr)s" \
        --apiserver-cert-extra-sans="%(addr)s"'
            % {"addr": api_server_addr}
        )
        sudo_cmd = ""
        if sudo:
            sudo_cmd = "sudo"
        pod_network = "--pod-network-cidr={cidr}".format(cidr=cidr)
        create_cmd = "{sudo} kubeadm init \
        --node-name k8s-master {api_server_settings} {pod_network}".format(
            sudo=sudo_cmd,
            api_server_settings=api_server_settings,
            pod_network=pod_network,
        )

        log.info("create Kubernetes cluster on '%s'", destination)
        ssh_session.execute_command(create_cmd)
        log.info("created Kubernetes cluster on '%s'", destination)

        # copy kube-config to local $HOME
        ssh_session.execute_command(
            command="if [ ! -d ~/.kube ];then mkdir ~/.kube; fi"
        )
        ssh_session.execute_command(
            command="{sudo} cp /etc/kubernetes/admin.conf ~/.kube/config".format(
                sudo=sudo_cmd
            )
        )
        ssh_session.execute_command(
            command="{sudo} chown $(id -u):$(id -g) ~/.kube/config".format(
                sudo=sudo_cmd
            )
        )

        ssh_session.execute_command(
            command="{sudo} mkdir -p -m 777 /mnt/pv1 /mnt/pv2".format(sudo=sudo_cmd)
        )

        kube_dir = os.path.expanduser("~/.kube")
        log.info("create Kubernetes config in '%s'", kube_dir)
        if not os.path.exists(kube_dir):
            os.mkdir(kube_dir)
        sftp = SFTPClient.from_transport(ssh_session.client.get_transport())
        try:
            sftp.get("./.kube/config", kube_dir + "/config")
        finally:
            sftp.close()
    finally:
        ssh_session.close()
