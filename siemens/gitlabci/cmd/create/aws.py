# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import logging
import os

import click

from siemens.gitlabci.cli import create as cli

if not os.getenv("_GITLABCI_COMPLETE", None):
    import sys
    import time

    import boto3
    import sh
    from sh import kops, kubectl
    from siemens.gitlabci.lib.aws import s3_exists_bucket
    from siemens.gitlabci.lib.deployment import bootstrap_helm
    from siemens.gitlabci.lib.kubernetes import (
        configure_kubernetes_client,
        add_dedicated_role_to_master,
        tolerate_deployment_on_master,
    )

log = logging.getLogger(__name__)


@cli.command()
@click.pass_context
@click.option(
    "--node-count",
    default=1,
    type=int,
    help="Initial K8s node count (>=1)",
    show_default=True,
)
@click.option(
    "--kubernetes-version",
    default="v1.18.14",
    help="Specify K8s version",
    show_default=True,
)
def aws(ctx, node_count, kubernetes_version):
    """Create ISAR CI setup running on AWS"""
    cfg = ctx.obj["cfg"]
    cluster_name = cfg.aws["cluster_name"]
    region = cfg.aws["region"]
    ssh_key = cfg.aws["ssh_key"]

    master_zones = cfg.aws["master_zones"]
    master_size = cfg.aws["master_size"]
    node_zones = cfg.aws["node_zones"]
    node_size = cfg.aws["node_size"]

    s3 = boto3.resource("s3", region_name=region)
    bucket_name = "{}-kops-state-store".format(cluster_name)
    state_store = "s3://{}".format(bucket_name)

    if not s3_exists_bucket(s3, bucket_name):
        log.info("Creating s3 bucket %s in region %s", bucket_name, region)
        s3.create_bucket(
            Bucket=bucket_name, CreateBucketConfiguration={"LocationConstraint": region}
        )
        log.info("Waiting for bucket creation to finish")
        s3.Bucket(bucket_name).wait_until_exists()

    full_cluster_name = "{}.k8s.local".format(cluster_name)
    if not is_cluster_up(node_count):
        log.info("Creating K8s master and nodes on AWS")
        kops(
            "create",
            "cluster",
            "--state",
            state_store,
            "--kubernetes-version",
            kubernetes_version,
            "--networking",
            "calico",
            "--zones",
            node_zones,
            "--master-zones",
            master_zones,
            "--master-size",
            master_size,
            "--node-count",
            max(1, node_count),  # 0 is not possible with kops
            "--node-size",
            node_size,
            "--authorization",
            "RBAC",
            "--cloud",
            "aws",
            "--ssh-public-key",
            ssh_key,
            "--yes",
            full_cluster_name,
            _out=sys.stdout,
        )
        kops(
            "create",
            "secret",
            "--state",
            state_store,
            "--name",
            full_cluster_name,
            "sshpublickey",
            "admin",
            "-i",
            ssh_key,
        )

        log.info(
            "Waiting for cluster to finish initialization. "
            "This takes about 5 minutes."
        )
        # wait 10min till master and nodes are running
        for i in range(0, 20):
            time.sleep(30)
            if is_cluster_up(node_count):
                break
            log.debug("Cluster is not yet up, waiting for another 30 seconds...")

    configure_kubernetes_client()

    tolerate_deployment_on_master("kube-dns", "kube-system")
    tolerate_deployment_on_master("kube-dns-autoscaler", "kube-system")
    tolerate_deployment_on_master("calico-kube-controllers", "kube-system")

    add_dedicated_role_to_master()

    bootstrap_helm()

    for i in range(0, 60):
        if is_cluster_up(node_count):
            break
        log.debug("Waiting for cluster to finish initialization")
        time.sleep(10)

    kubectl("create", "-f", "./share/k8s/dashboard/dashboard-admin.yaml")

    kops("validate", "--state", state_store, "cluster", "--wait", "10m")
    log.info("Cluster is up and running. Validating cluster status.")


def is_cluster_up(node_count):
    """
    check if the cluster is up by getting the status of the master.
    The status is True if the master is ready.
    """
    try:
        # get the status of the last message
        status = kubectl(
            "get", "nodes", "-o", "custom-columns=:.status.conditions[-1].status"
        )
        node_status = status.stdout.decode("utf-8").strip().split()
        # check if all nodes are running
        if len(node_status) <= max(1, node_count):
            return False
        for state in node_status:
            if state != "True":
                return False
        return True
    except sh.ErrorReturnCode:
        return False
