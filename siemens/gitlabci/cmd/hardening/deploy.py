# Copyright (c) Siemens AG, 2020
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#
import os
import tempfile
import logging

from string import Template

import click

from siemens.gitlabci.cli import hardening as cli

if not os.getenv("_GITLABCI_COMPLETE", None):
    import kubernetes
    from siemens.gitlabci.lib.ansible import install_role, run_playbook
    from siemens.gitlabci.lib.kubernetes import configure_kubernetes_client


log = logging.getLogger(__name__)


@cli.command()
@click.pass_context
@click.option(
    "--inventory",
    default="./share/k8s/hardening/inventory",
    help="Specify the ansible inventory",
    show_default=True,
)
@click.option(
    "--playbook",
    default="./share/k8s/hardening/hardening.yaml",
    help="Specify the playbook to harden the Kubernetes cluster",
    show_default=True,
)
@click.option(
    "--user", default="ubuntu", help="Specify the ansible user", show_default=True
)
@click.option(
    "--key-file",
    required=True,
    help="Specify the key-file to access the kubernetes cluster",
)
def deploy(ctx, inventory, key_file, playbook, user):
    """deploy hardening"""

    install_role("dev-sec.os-hardening")
    install_role("dev-sec.ssh-hardening")

    configure_kubernetes_client()

    k8s_client_core = kubernetes.client.CoreV1Api()
    hosts = []
    # get all hosts with external dns
    for row in k8s_client_core.list_node().items:
        addresses = row.status.addresses
        for address in addresses:
            if address.type == "ExternalDNS":
                hosts.append(address.address)

    with open(playbook, mode="r") as playbook_file:
        ansible_playbook = Template(playbook_file.read())

    with open(inventory, mode="r") as inventory_file:
        ansible_inventory = Template(inventory_file.read())

    for host in hosts:
        overwrites = {"host": host, "user": user, "key_file": key_file}

        with tempfile.NamedTemporaryFile(
            mode="w"
        ) as temp_playbook, tempfile.NamedTemporaryFile(mode="w") as temp_inventory:
            merged_playbook = ansible_playbook.substitute(overwrites)
            merged_inventory = ansible_inventory.substitute(overwrites)
            temp_playbook.write(merged_playbook)
            temp_inventory.write(merged_inventory)
            temp_playbook.flush()
            temp_inventory.flush()
            run_playbook(temp_playbook.name, temp_inventory.name)
