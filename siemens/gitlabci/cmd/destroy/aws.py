# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import logging
import os

import click

from siemens.gitlabci.cli import destroy

if not os.getenv("_GITLABCI_COMPLETE", None):
    import sys

    import boto3
    from sh import kops

    from siemens.gitlabci.lib.aws import (
        aws_iam_lookup_policy_arn,
        delete_autoscaler_iam,
        s3_exists_bucket,
    )

log = logging.getLogger(__name__)


@destroy.command("aws")
@click.pass_context
def destroy(ctx):
    """Destroy ISAR CI setup running on AWS"""

    cfg = ctx.obj["cfg"]
    cluster_name = cfg.aws["cluster_name"]
    region = cfg.aws["region"]

    bucket_name = "{}-kops-state-store".format(cluster_name)
    state_store = "s3://{}".format(bucket_name)

    full_cluster_name = "{}.k8s.local".format(cluster_name)
    log.info("Tearing down cluster %s on AWS...", full_cluster_name)

    asg_policy_name = cfg.aws["asg_policy_name"]
    delete_autoscaler_iam(cluster_name, region, asg_policy_name)
    kops(
        "delete",
        "cluster",
        "--state",
        state_store,
        "--yes",
        "--name",
        full_cluster_name,
        _out=sys.stdout,
    )
    log.info("Cluster %s has been destroyed successfully", full_cluster_name)

    s3 = boto3.resource("s3", region_name=region)
    if s3_exists_bucket(s3, bucket_name):
        log.info("Deleting s3 bucket %s in region %s", bucket_name, region)
        s3.Bucket(bucket_name).delete()

    iam = boto3.client("iam", region_name=region)
    asg_policy_name = cfg.aws["asg_policy_name"]
    policy_arn = aws_iam_lookup_policy_arn(iam, asg_policy_name)
    if policy_arn:
        log.info("Deleting IAM policy %s", policy_arn)
        iam.delete_policy(PolicyArn=policy_arn)
