# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import json


class Config:
    def __init__(self, fpath):
        with open(fpath) as f:
            self.__cfg = json.load(f)

    def __getattr__(self, name):
        return self.__cfg[name]
