# Copyright (c) Siemens AG, 2020
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

{ pkgs ? import <nixpkgs> { } }:

with pkgs;

poetry2nix.mkPoetryApplication {

  projectDir = ./.;

}
